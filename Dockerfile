FROM node:latest


WORKDIR /user/src/app

COPY ./app/package*.json ./

RUN npm install -g @angular/cli
RUN npm install

CMD ng add @nguniversal/express-engine

COPY ./app ./

EXPOSE 4000

CMD npm run build:ssr && npm run serve:ssr