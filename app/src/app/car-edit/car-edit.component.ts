import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "../api.service";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormsModule
} from "@angular/forms";

@Component({
  selector: "app-car-edit",
  templateUrl: "./car-edit.component.html",
  styleUrls: ["./car-edit.component.css"]
})
export class CarEditComponent implements OnInit {
  submitted = false;
  editForm: FormGroup;
  make;
  id;
  car;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    public formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      model: [""],
      year: ["", [Validators.pattern("^[0-9]{4}$")]]
    });
    this.activatedRoute.paramMap.subscribe(params => {
      console.log(params.get("make"));
      this.make = params.get("make");
      this.id = params.get("id");
      this.apiService
        .getSpecificCar(this.make, this.id)
        .subscribe(async data => {
          this.car = await data;
          console.log(this.car);
          this.editForm.setValue({
            model: this.car.MODEL.S,
            year: this.car.YEAR.N
          });
        });
    });
  }

  onSubmit() {
    this.submitted = true;
    if (!this.editForm.valid) {
      return false;
    } else {
      if (window.confirm("Are you sure?")) {
        this.editForm.value.model = this.editForm.value.model
          .toLowerCase()
          .trim();
        console.log(this.editForm.value);
        this.apiService
          .updateCar(this.make, this.id, this.editForm.value)
          .subscribe(
            res => {
              this.router.navigateByUrl("/");
              console.log("Car updated successfully");
            },
            error => {
              console.log(error);
            }
          );
      }
    }
  }
}
