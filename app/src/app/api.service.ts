import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  baseUri: string = "http://localhost:8080/rest";
  headers = new HttpHeaders().set("Content-Type", "application/json");

  constructor(private http: HttpClient) {}

  //Get all cars list
  getAllCars(): Observable<any> {
    let url = `${this.baseUri}/cars`;
    return this.http.get(url);
  }

  getSpecificCar(make, id): Observable<any> {
    let url = `${this.baseUri}/cars/${make}/${id}`;
    return this.http
      .get(url, { headers: this.headers })
      .pipe(catchError(this.errorMgmt));
  }

  createCar(data): Observable<any> {
    let url = `${this.baseUri}/cars`;
    return this.http.post(url, data).pipe(catchError(this.errorMgmt));
  }

  updateCar(make, id, data) {
    let url = `${this.baseUri}/cars/${make}/${id}`;
    return this.http
      .put(url, data, { headers: this.headers })
      .pipe(catchError(this.errorMgmt));
  }

  deleteCar(make, id): Observable<any> {
    let url = `${this.baseUri}/cars/${make}/${id}`;
    return this.http
      .delete(url, { headers: this.headers })
      .pipe(catchError(this.errorMgmt));
  }

  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
