export const cars = [
  {
    id: "1",
    make: "Honda",
    model: "civic",
    year: "2008"
  },

  {
    id: "2",
    make: "Honda",
    model: "accord",
    year: "2008"
  },

  {
    id: "3",
    make: "Honda",
    model: "crv",
    year: "2008"
  }
];
