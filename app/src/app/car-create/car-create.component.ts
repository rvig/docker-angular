import { Component, OnInit, NgZone } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ApiService } from "../api.service";
import { from } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "app-car-create",
  templateUrl: "./car-create.component.html",
  styleUrls: ["./car-create.component.css"]
})
export class CarCreateComponent implements OnInit {
  submitted = false;
  carForm;

  constructor(
    private apiService: ApiService,
    public formBuilder: FormBuilder,
    private ngZone: NgZone,
    private router: Router
  ) {
    this.carForm = this.formBuilder.group({
      make: ["", [Validators.required]],
      model: ["", [Validators.required]],
      year: ["", [Validators.required, Validators.pattern("^[0-9]{4}$")]]
    });
  }

  ngOnInit(): void {}

  get myForm() {
    return this.carForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.carForm.valid) {
      return false;
    } else {
      this.carForm.value.make = this.carForm.value.make.toLowerCase().trim();
      this.carForm.value.model = this.carForm.value.model.toLowerCase().trim();
      this.apiService.createCar(this.carForm.value).subscribe(
        res => {
          console.log(this.carForm.value);
          console.log("Car created successfully");
          this.ngZone.run(() => {
            this.router.navigateByUrl("/");
          });
        },
        error => {
          console.log(error);
        }
      );
    }
  }
}
