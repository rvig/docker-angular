import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CarsListComponent } from "./cars-list/cars-list.component";
import { CarCreateComponent } from "./car-create/car-create.component";
import { CarEditComponent } from "./car-edit/car-edit.component";

const routes: Routes = [
  { path: "", component: CarsListComponent },
  { path: "create", component: CarCreateComponent },
  { path: "edit/:make/:id", component: CarEditComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
