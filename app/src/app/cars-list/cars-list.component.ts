import { Component, OnInit } from "@angular/core";

import { cars } from "../cars";
import { ApiService } from "../api.service";

@Component({
  selector: "app-cars-list",
  templateUrl: "./cars-list.component.html",
  styleUrls: ["./cars-list.component.css"]
})
export class CarsListComponent implements OnInit {
  cars;
  car;
  searchText;
  constructor(private apiService: ApiService) {
    this.apiService.getAllCars().subscribe(data => {
      if (data == "No cars in inventory") {
        this.cars = "No cars in inventory";
      } else {
        this.cars = data;
      }
    });
  }

  deleteCar(make, id) {
    if (window.confirm("Are you sure?")) {
      this.apiService.deleteCar(make, id).subscribe(data => {
        this.car = data;
        this.cars = this.cars.filter(car => {
          if (car.MAKE.S == make && car.CAR_ID.N == id) {
            return false;
          } else {
            return true;
          }
        });

        if (this.cars.length == 0) {
          console.log(true);
          this.cars = "No cars in inventory";
        }

        console.log(data);
      });
    }
  }

  ngOnInit(): void {}
}
